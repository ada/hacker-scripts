=============
hacker-script
=============

*collection of scripts based on https://github.com/NARKOZ/hacker-scripts*


*List of scripts*:


``email_remainder.py``
      In case if I did not received email from on of *${EMAIL_REMINDER_PERSONS_EMAIL}*
      
      I have to remind them about missed email in 'my' and
      send to remind later to each *${EMAIL_REMINDER_PERSONS_EMAIL}* again