#!/usr/bin/env python

"""\
===================
Hallo

This is reminder ROBOT script:
    https://bitbucket.org/ada/hacker-scripts/src/HEAD/email_remainder.py

If you received this mail then you promise to send something and forgot this.
Please fulfill your promise and you will stop send you remind email
=========================

Technical details::
+++++++++++++++++++

In case if I did not received email from on of ${EMAIL_REMINDER_PERSONS_EMAIL}
I have to remind them about missed email in 'my' and
send to remind later to each ${EMAIL_REMINDER_PERSONS_EMAIL} again

Implementation of:
    https://developers.google.com/gmail/api/quickstart/python

Usage:
    # create ~/client_secret.json
    export EMAIL_REMINDER_PERSONS_EMAIL="a@mail.org OR b@mail.org"    
    python email_remainder.py

    Run forever
    . /opt/python-env/haker-scripts/bin/activate # python3
    ./email_remainder.py run_forever &>> /tmp/email_remainder.log

"""
from email.mime.text import MIMEText
import os
import base64
import argparse
import sys
import datetime
import time
import logging

import httplib2
from apiclient import discovery
import oauth2client
from oauth2client import client
from oauth2client import tools


def get_logger(name, formatter=None, handler=None):
    logger = logging.getLogger(name)
    logger.setLevel(logging.INFO)
    handler = handler or logging.StreamHandler(sys.stdout)
    handler.setFormatter(logging.Formatter(
        formatter or 
        "%(asctime)s [%(levelname)s] %(funcName)s(%(lineno)d) - %(message)s"
    ))
    logger.addHandler(handler)
    
    return logger


CLIENT_SECRET_FILE = '~/client_secret.json'
"""
$ cat ~/client_secret.json
{
  "web": {
    "client_id": "33445779-gr....apps.googleusercontent.com",
    "client_secret": "mhshcJ3jpo2hFrC29NMpTiAK",
    "redirect_uris": ["https://www.example.com/oauth2callback"],
    "auth_uri": "https://accounts.google.com/o/oauth2/auth",
    "token_uri": "https://accounts.google.com/o/oauth2/token"
  }
}
"""

APPLICATION_NAME = 'Gmail API Python Quickstart'
SCOPES = [
    "https://www.googleapis.com/auth/gmail.readonly",
    "https://www.googleapis.com/auth/gmail.send"
]


def get_credentials():
    """Gets valid user credentials from storage.

    If nothing has been stored, or if the stored credentials are invalid,
    the OAuth2 flow is completed to obtain the new credentials.

    Returns:
        Credentials, the obtained credential.
    """
    home_dir = os.path.expanduser('~')
    credential_dir = os.path.join(home_dir, '.credentials')
    if not os.path.exists(credential_dir):
        os.makedirs(credential_dir)
    credential_path = os.path.join(
        credential_dir,
        'gmail-python-quickstart.json'
    )

    store = oauth2client.file.Storage(credential_path)
    credentials = store.get()
    if not credentials or credentials.invalid:
        flow = client.flow_from_clientsecrets(
            os.path.expanduser(CLIENT_SECRET_FILE),
            " ".join(SCOPES)
        )
        flow.user_agent = APPLICATION_NAME
        credentials = tools.run_flow(flow, store, FLAGS)

        LOGGER.info('Storing credentials to `%s`', credential_path)

    return credentials


def get_service():
    credentials = get_credentials()
    http = credentials.authorize(httplib2.Http())
    return discovery.build('gmail', 'v1', http=http)


def last_email(service, results):
    return service.users().messages().get(
        userId='me', id=results["messages"][0]["id"], format="minimal"
    ).execute()


def date_of_last_reminder(query):
    service = get_service()

    results = service.users().messages().list(userId='me', q=query).execute()

    email = last_email(service, results)

    if email:
        return(datetime.datetime.fromtimestamp(
            int(email["internalDate"]) / 1000
        ))


def send_remind_email():
    """Shows basic usage of the Gmail API.

    Creates a Gmail API service object and outputs a list of label names
    of the user's Gmail account.
    """
    service = get_service()

    query = "is:unread from:(%(EMAIL_REMINDER_PERSONS_EMAIL)s)" % os.environ
    LOGGER.info("query: `%s`", query)

    results = service.users().messages().list(userId='me', q=query).execute()

    if not results["resultSizeEstimate"]:
        LOGGER.info("did not received any email")

        message = MIMEText(__doc__)
        message['to'] = ",".join(
            os.environ["EMAIL_REMINDER_PERSONS_EMAIL"].split(" OR ")
        )

        message['subject'] = "Remind"

        message_string = message.as_string().encode("utf-8")
        message_string = base64.b64encode(message_string).decode("utf-8")

        body = {'raw': message_string}

        message = service.users().messages().send(
            userId="me",
            body=body
        ).execute()

        LOGGER.info(message)

    else:
        email = last_email(service, results)

        LOGGER.info("last email was `%s`", email)
        LOGGER.info(
            "did received `%d` emails, from `%s`",
            results["resultSizeEstimate"],
            os.environ["EMAIL_REMINDER_PERSONS_EMAIL"]
        )


def throttling(callback):
    now = datetime.datetime.now()

    if 10 < now.hour < 18:
        if 0 < now.isoweekday() < 6:
            date = date_of_last_reminder(
                "subject:remind to:(%s)" %
                os.environ["EMAIL_REMINDER_PERSONS_EMAIL"]
            )
            diff = datetime.datetime.now() - date
            if diff > datetime.timedelta(hours=3):
                callback()
            else:
                LOGGER.info("wait at least 3 hours to send next reminder")
        else:
            LOGGER.info("waiting working days Monday-Friday")
    else:
        LOGGER.info("waiting working time 10-18 o'clock")


if __name__ == '__main__':
    LOGGER = get_logger(__name__)
    LOGGER.info("============================================================")
    LOGGER.info("start at `%s`", datetime.datetime.now())
    try:
        COMMAND = sys.argv[1]
    except IndexError as no_command:
        LOGGER.exception(no_command)
        FLAGS = argparse.ArgumentParser(
            parents=[tools.argparser]
        ).parse_args()

        send_remind_email()
    else:
        if COMMAND == "run":
            throttling(send_remind_email)
        elif COMMAND == "run_forever":
            while True:
                throttling(send_remind_email)
                LOGGER.info("sleep 20 minutes")
                time.sleep(60 * 20)  # 20 minutes
        else:
            LOGGER.fatal("expect only `run` command bu got `%s`", COMMAND)

    LOGGER.info("end at %s", datetime.datetime.now())
